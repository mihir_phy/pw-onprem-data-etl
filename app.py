import utils
import data_validation as dv


def lambda_handler(event, _):
    print("event :: {}".format(event))
    # get bucket and object key
    bucket_name = event["Records"][0]["s3"]["bucket"]["name"]
    object_key = event["Records"][0]["s3"]["object"]["key"]
    print("bucket :: {} || object key :: {}".format(bucket_name, object_key))

    # get company name
    company_name = object_key.split("/")[2]
    print("company_name :: ", company_name)

    # skip if demo account
    if company_name == "demo":
        print("skipping demo account")
        return {"success": True, "error": "skipping demo account"}

    # read csv file from s3 bucket
    df = utils.read_csv_from_s3(bucket_name=bucket_name, object_key=object_key)
    if not df.empty:
        # convert csv to dict
        df_values = df.to_dict(orient="records")

        # Validate the new entry
        is_valid = dv.validate_new_records(values=df_values, company=company_name)

        if not is_valid:
            print("records validation failed")
            return {"success": False, "error": "records validation failed"}

        # get the latest master file
        master_df = utils.get_latest_master_csv(company=company_name)
        if not master_df.empty:
            # TODO: change this to take actions as per object tags
            # append new data to master csv
            final_df = utils.append_data_to_master_csv(new_df=df, master_df=master_df)
            if final_df.empty:
                print("unable to append csv")
                return {"success": False, "error": "unable to append csv"}
        else:
            print("unable to read master csv")
            final_df = df

        if utils.save_csv_to_s3(df=final_df, company=company_name):
            print("ETL completed")
            return {"success": True, "error": ""}
    else:
        print("unable to read new csv")
        return {"success": False, "error": "Unable to read csv file"}


if __name__ == "__main__":
    event = {
        "Records": [
            {
                "s3": {
                    "bucket": {
                        "name": "pw-raw-data-test",
                    },
                    "object": {
                        "key": "algalif/csv/manual/1645817189.csv",
                    },
                },
            }
        ]
    }

    lambda_handler(event=event, _=None)
