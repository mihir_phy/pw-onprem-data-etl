FROM public.ecr.aws/lambda/python:3.8

RUN yum update -y \
    && yum install -y mesa-libGL \
    && pip install --upgrade pip

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . ./

CMD ["app.lambda_handler"]